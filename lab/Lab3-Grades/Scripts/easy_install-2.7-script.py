#!D:\STUDIA\ROK3~1\SEMEST~2\PROGRA~1\lab3\lab\LAB3-G~1\Scripts\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'setuptools==1.1.5','console_scripts','easy_install-2.7'
__requires__ = 'setuptools==1.1.5'
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.exit(
        load_entry_point('setuptools==1.1.5', 'console_scripts', 'easy_install-2.7')()
    )
