# -*- encoding: utf-8 -*-

import getpass
import requests
from bs4 import BeautifulSoup
import Obiekt

url = {
    'login': 'https://canvas.instructure.com/login',
    'grades': 'https://canvas.instructure.com/courses/838324/grades'
}

username = raw_input('Użytkownik: ')
password = getpass.getpass('Hasło: ')

payload = {
    'pseudonym_session[unique_id]': username,
    'pseudonym_session[password]': password
}

# Logowanie

session = requests.session()
session.post(url['login'], data=payload)


# Pobranie strony z ocenami

response = session.get(url['grades'])


# Odczyt strony
html = response.text

# Parsowanie strony
parsed = BeautifulSoup(html, 'html5lib', )

# Scraping
grades = parsed.find(id='grades_summary', recursive=True)
#print grades
ctx1 = grades.find_all('div', 'context', recursive=True)

wiersze = grades.find_all('tr', 'student_assignment', recursive=True)

laborki= []
aktywnosc= []
pd = []

for s in wiersze:

    nazwa = s.find('div', 'context', recursive=True)
    print nazwa

    if not nazwa:
        continue

    ocena=s.find('span', 'score', recursive=True)

    if not ocena:
        ocena = 0
    #print ocena
    link= s.find('a')
    o = Obiekt.Obiekt(nazwa.text, link.text, ocena.text)
    if nazwa.text == 'Laboratoria':
        laborki.append(o)
    elif nazwa.text == 'Praca domowa':
        pd.append(o)
    else:
        aktywnosc.append(o)



print laborki[1].nazwa
print laborki[1].ocena


# Sortowanie

# Wyświetlenie posortowanych ocen w kategoriach