
import unittest
import FindToilet

class Tests(unittest.TestCase):
    def test_incorrect_input(self):
        a=' '
        first ='Podany adres jest niepoprawny!'
        second = FindToilet.FindToilet(a)
        self.assertEqual(first,second)

    def test_correct_input(self):
        a='Dolna, Warszawa, Polska '
        first ='Done'
        second = FindToilet.FindToilet(a)
        self.assertEqual(first,second)

    def test_api_key(self):
        self.assertTrue(FindToilet.google_places)

