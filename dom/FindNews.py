# -*- encoding: utf-8 -*-

import requests
from bs4 import BeautifulSoup


def FindNews():

    is_work = True

    while is_work == True:
        print 'Wiadomości z :'
        print '1 - Kraju'
        print '2 - Świata'
        print '0 - Wróć do głownego menu'
        wybor = raw_input('Wybierz rodziaj wiadomości : ')
        print '\r'

        if wybor == '1' or wybor == '2' or wybor == '0':
            if wybor == '1':
                session = requests.session()
                response = session.get('http://wiadomosci.onet.pl/kraj')
                try:
                    News(response)
                except:
                    continue
                session.close()
            elif wybor == '2':
                session = requests.session()
                response = session.get('http://wiadomosci.onet.pl/swiat')
                try:
                    News(response)
                except:
                    continue
                session.close()
            elif wybor == '0':
                is_work = False
        else:
            print 'Nie ma takiej opcji :D \r\n'

def News(response):

        html = response.text

        parsed = BeautifulSoup(html, 'html5lib')
        news = parsed.find('div', 'staticStreamContent', recursive=True)
        titles = news.find_all('h3', 'itemTitle', recursive=True)

        print "Wiadomości onet.pl : \r\n"
        i = 0
        while i < len(titles):
            tempTitle = str(titles[i])[23:].split(' <')
            title = tempTitle[0]
            link = news.find('a', title=title)

            if link:
                descripsionTemp = link.find('div','itemLead hyphenate')
                if descripsionTemp:
                    descripsion = descripsionTemp.text[28:]
                    print title, ":"
                    if not descripsion[:1] == '-':
                        print descripsion
                    else:
                        print descripsion[2:]
            i += 1